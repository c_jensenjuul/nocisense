package nocisense;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

/**
 * A simple demonstration application showing how to create a bar chart.
 */
public class BarChart extends ApplicationFrame {
    
    // ArrayList with analyzed data from file/database. High and Low counters for all Sensors
    private static HashMap<String, ArrayList<Integer>> analyzedMeasurements;
    private static int counter = 0;
    private static ChartPanel chartPanel;

    public BarChart(String title, HashMap<String, ArrayList<Integer>> analyzedMeasurementsX, int counter) {

        super(title);
        this.analyzedMeasurements = analyzedMeasurementsX;
        this.counter = counter; // values defined by usen in MainGUI window -> Counter textfield
        CategoryDataset dataset = createDataset(); // creates dataset for BarChart
        JFreeChart chart = createChart(dataset);
        chartPanel = new ChartPanel(chart, false);
        chartPanel.setPreferredSize(new Dimension(500, 270));
        setContentPane(chartPanel);


       // this.analyzedMeasurements = analyzedMeasurements;
    }

    public BarChart(String title) {

        super(title);
        CategoryDataset dataset = createEmptyDataset();
        JFreeChart chart = createChart(dataset);
        chartPanel = new ChartPanel(chart, false);
        chartPanel.setPreferredSize(new Dimension(500, 270));
        setContentPane(chartPanel);

        //showChart();

       // this.analyzedMeasurements = analyzedMeasurements;
    }
    
    public static ChartPanel getChartPAnel()
    {
        return chartPanel;
    }


    private static CategoryDataset createDataset() {

        String lowCounter = "Low Counter"; // info for Chart legend
        String highCounter = "High Counter"; // info for Chart legend
        String series3 = "Counter"; // info for Chart legend

        // column keys...
        String l1 = "L1";
        String l2 = "L2";
        String l3 = "L3";
        String l4 = "L4";
        String l5 = "L5";
        String l6 = "L6";

        String delimiterLeft = "  ";
        String counterText = "Counter";
        String delimiterRight = " ";

        String r1 = "R1";
        String r2 = "R2";
        String r3 = "R3";
        String r4 = "R4";
        String r5 = "R5";
        String r6 = "R6";

        // create the dataset...
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        dataset.addValue(analyzedMeasurements.get("L1").get(1), lowCounter, l1);
        dataset.addValue(analyzedMeasurements.get("L2").get(1), lowCounter, l2);
        dataset.addValue(analyzedMeasurements.get("L3").get(1), lowCounter, l3);
        dataset.addValue(analyzedMeasurements.get("L4").get(1), lowCounter, l4);
        dataset.addValue(analyzedMeasurements.get("L5").get(1), lowCounter, l5);
        dataset.addValue(analyzedMeasurements.get("L6").get(1), lowCounter, l6);

        dataset.addValue(null, lowCounter, delimiterLeft);
        dataset.addValue(counter, series3, counterText);
        dataset.addValue(null, lowCounter, delimiterRight);

        dataset.addValue(analyzedMeasurements.get("R1").get(1), lowCounter, r1);
        dataset.addValue(analyzedMeasurements.get("R2").get(1), lowCounter, r2);
        dataset.addValue(analyzedMeasurements.get("R3").get(1), lowCounter, r3);
        dataset.addValue(analyzedMeasurements.get("R4").get(1), lowCounter, r4);
        dataset.addValue(analyzedMeasurements.get("R5").get(1), lowCounter, r5);
        dataset.addValue(analyzedMeasurements.get("R6").get(1), lowCounter, r6);
        
        dataset.addValue(analyzedMeasurements.get("L1").get(0), highCounter, l1);
        dataset.addValue(analyzedMeasurements.get("L2").get(0), highCounter, l2);
        dataset.addValue(analyzedMeasurements.get("L3").get(0), highCounter, l3);
        dataset.addValue(analyzedMeasurements.get("L4").get(0), highCounter, l4);
        dataset.addValue(analyzedMeasurements.get("L5").get(0), highCounter, l5);
        dataset.addValue(analyzedMeasurements.get("L6").get(0), highCounter, l6);

        dataset.addValue(null, highCounter, delimiterLeft);
        dataset.addValue(null, highCounter, delimiterRight);

        dataset.addValue(analyzedMeasurements.get("R1").get(0), highCounter, r1);
        dataset.addValue(analyzedMeasurements.get("R2").get(0), highCounter, r2);
        dataset.addValue(analyzedMeasurements.get("R3").get(0), highCounter, r3);
        dataset.addValue(analyzedMeasurements.get("R4").get(0), highCounter, r4);
        dataset.addValue(analyzedMeasurements.get("R5").get(0), highCounter, r5);
        dataset.addValue(analyzedMeasurements.get("R6").get(0), highCounter, r6);

        return dataset;

    }

    private static CategoryDataset createEmptyDataset() {

        String lowCounter = "Low Counter";
        String highCounter = "High Counter";
        String series3 = "Counter";

        String delimiterLeft = "  ";
        String counterText = "Counter";
        String delimiterRight = " ";

        // create the dataset...
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i = 1; i < 7; i++) {
            dataset.addValue(0, lowCounter, "L" + i);
        }

        dataset.addValue(null, lowCounter, delimiterLeft);
        dataset.addValue(null, series3, counterText);
        dataset.addValue(null, lowCounter, delimiterRight);

        for (int i = 1; i < 7; i++) {
            dataset.addValue(0, lowCounter, "R" + i);
        }

        for (int i = 1; i < 7; i++) {
            dataset.addValue(0, highCounter, "L" + i);
        }

        dataset.addValue(null, highCounter, delimiterLeft);
        dataset.addValue(null, highCounter, delimiterRight);

        for (int i = 1; i < 7; i++) {
            dataset.addValue(0, highCounter, "R" + i);
        }
        return dataset;
    } // Create empty chart. Shown when application is loaded

    /**
     * Creates a sample chart.
     *
     * @param dataset the dataset.
     *
     * @return The chart.
     */
    private static JFreeChart createChart(CategoryDataset dataset) {

        // create the chart...
        JFreeChart chart = ChartFactory.createBarChart(
                "Measurements", // chart title
                "Sensors", // domain axis label
                "Counts", // range axis label
                dataset, // data
                PlotOrientation.VERTICAL, // orientation
                true, // include legend
                true, // tooltips?
                false // URLs?
        );

        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
        // set the background color for the chart...
        chart.setBackgroundPaint(new Color(214, 217, 223));


        // get a reference to the plot for further customisation...
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(new Color(240, 242, 246));
        plot.setDomainGridlinePaint(Color.black);
        plot.setDomainGridlinesVisible(true);
        plot.setRangeGridlinePaint(Color.black);

        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        // disable bar outlines...
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);

        // set up gradient paints for series...
        GradientPaint gp0 = new GradientPaint(
                0.0f, 0.0f, Color.yellow,
                0.0f, 0.0f, new Color(0, 0, 64)
        );
        GradientPaint gp1 = new GradientPaint(
                0.0f, 0.0f, Color.green,
                0.0f, 0.0f, new Color(0, 64, 0)
        );
        GradientPaint gp2 = new GradientPaint(
                0.0f, 0.0f, Color.red,
                0.0f, 0.0f, new Color(64, 0, 0)
        );
        renderer.setSeriesPaint(0, gp0);
        renderer.setSeriesPaint(1, gp1);
        renderer.setSeriesPaint(2, gp2);

        CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
        );
        // OPTIONAL CUSTOMISATION COMPLETED.
        
        
        // CREATE SCREENSHOT OF THE CHART     
        try {
            final ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
            final File file1 = new File("chartScreenshot.png");
            ChartUtilities.saveChartAsPNG(file1, chart, 500, 400, info);
            } catch (Exception e) {
        }
        return chart;
    }
}
