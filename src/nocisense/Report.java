/*
 This class handles everything to do with reports of the analyses done with a specific data set.
 */
package nocisense;

import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author Christina
 */
public class Report {

    // Creates a report in a textfile, saved in a directory chosen by the user
    public static void createReport(String name, String timeStamp, String counter,
            String lowThreshold, String highThreshold, HashMap<String, ArrayList<Integer>> mapCount) {

        timeStamp = timeStamp.replace(':', '.');
        try {
            JFileChooser f = new JFileChooser();
            f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            f.showSaveDialog(null);
            PrintStream output = new PrintStream(new File(f.getSelectedFile() + "\\Report " + name + " " + timeStamp + ".txt"));

            output.printf("Patient: %24s", name);
            output.println();
            output.printf("Date and time:  %17s", timeStamp);
            output.println();
            output.println();
            output.printf("Counter:        %17s", counter);
            output.println();
            output.printf("Low Threshold:  %17s", lowThreshold);
            output.println();
            output.printf("High Threshold: %17s", highThreshold);
            output.println();
            output.println();
            output.println();
            output.println("===================================");
            output.printf(" %18s%15s", "Low Count", "High Count");
            output.println();
            output.println("===================================");
            ArrayList<String> sensors = new ArrayList<>(mapCount.keySet());
            Collections.sort(sensors);
            for (String sensor : sensors) {
                output.printf(" %s:%15d%15d", sensor, mapCount.get(sensor).get(1), mapCount.get(sensor).get(0));
                output.println();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Creates a report in a pdf file, saved in a directory chosen by the user
    public static void createPdfReport(String nameX, String timeStampX, String counterX,
            String lowThresholdX, String highThresholdX, HashMap<String, ArrayList<Integer>> mapCount) {

        timeStampX = timeStampX.replace(':', '.');

        //PDF PART
        try {
            JFileChooser f = new JFileChooser();
            f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            f.showSaveDialog(null);
            com.itextpdf.text.Document document = new com.itextpdf.text.Document();               
            PdfWriter.getInstance(document, new FileOutputStream(f.getSelectedFile() + "\\Report " + nameX + " " + timeStampX + ".pdf"));
            document.open();
            Image logo = Image.getInstance("logoForReport.png");
            logo.setAbsolutePosition(450f, 700f);
            document.add(logo);
            String name         = "Patient: " + nameX;
            document.add(new Paragraph(name));
            String timeStamp    = "Date and time: " + timeStampX;
            document.add(new Paragraph(timeStamp));
            String counter      = "Counter: " + counterX;
            document.add(new Paragraph(counter));
            String lowThreshold = "Low Threshold: " + lowThresholdX;
            document.add(new Paragraph(lowThreshold));
            String highThreshold = "High Threshold: " + highThresholdX;
            document.add(new Paragraph(highThreshold));
            String space = "\n\n";
            document.add(new Paragraph(space));

            // TABLE            
            PdfPTable table = new PdfPTable(3); // 3 columns.
            table.setWidthPercentage(60);
            float[] columnWidths = {2f, 4f, 4f};
            table.setWidths(columnWidths);    
            PdfPCell cell1 = new PdfPCell(new Paragraph("Sensors"));
            PdfPCell cell2 = new PdfPCell(new Paragraph("Low Counter"));
            PdfPCell cell3 = new PdfPCell(new Paragraph("High Counter"));
            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            document.add(table);
            
            ArrayList<String> sensors = new ArrayList<>(mapCount.keySet());
            Collections.sort(sensors);
            for (String sensor : sensors) {
                PdfPTable tableX = new PdfPTable(3); // 3 columns.
                tableX.setWidthPercentage(60);
                tableX.setWidths(columnWidths);
                PdfPCell cell4 = new PdfPCell(new Paragraph(sensor+":"));
                PdfPCell cell5 = new PdfPCell(new Paragraph(mapCount.get(sensor).get(1)+""));
                PdfPCell cell6 = new PdfPCell(new Paragraph(mapCount.get(sensor).get(0)+""));
                tableX.addCell(cell4);
                tableX.addCell(cell5);
                tableX.addCell(cell6);  
                document.add(tableX);
            }            
            //break
            document.add(new Paragraph(space));
            
            // IMAGE
            Image image = Image.getInstance("chartScreenshot.png");
            //image.setAbsolutePosition(400f, 750f);
            document.add(image);
            document.close();

        } catch (Exception e) {
        }
    }
}
