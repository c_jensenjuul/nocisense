package nocisense;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnector {
    
    private static final String USER = "nocisense"; 
    private static final String PASS = "Cheburashka17";  
    private static final String URL = "jdbc:mysql://138.128.216.12/nocisense";
    
    private static final String USERLOCAL = "root"; 
    private static final String PASSLOCAL = "";  
    private static final String URLLOCAL  = "jdbc:mysql://localhost:3306/nocisense";
    
    
    public static Connection getConnection() {   
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(URL, USER, PASS);
        } catch (Exception ex) {
            try {
                conn = DriverManager.getConnection(URLLOCAL, USERLOCAL, PASSLOCAL);
                System.out.println("Saving to local database");
            } catch (Exception e) {
                System.err.println("Error conecting to local database");
            } 
            System.err.println("Error conecting to database");
        } 
        return conn;
    }
}
