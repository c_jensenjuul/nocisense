package nocisense;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import static nocisense.MainGUI.filePath;

/**
 *
 * @author Christina
 */
public class ImportNewData {

    
    private static ArrayList<String> timeStamps = new ArrayList<>();
    

    public static ArrayList<String> getTimeStamps()
    {
        return timeStamps;
    }
    
    public static HashMap<String, ArrayList<Double>> ImportDataFromFile() {

        ArrayList<Double> valuesL1 = new ArrayList();// create ArrayList to store measurements from Left foot Sensor 1
        ArrayList<Double> valuesL2 = new ArrayList();// create ArrayList to store measurements from Left foot Sensor 2
        ArrayList<Double> valuesL3 = new ArrayList();//....
        ArrayList<Double> valuesL4 = new ArrayList();
        ArrayList<Double> valuesL5 = new ArrayList();
        ArrayList<Double> valuesL6 = new ArrayList();
        ArrayList<Double> valuesR1 = new ArrayList();
        ArrayList<Double> valuesR2 = new ArrayList();
        ArrayList<Double> valuesR3 = new ArrayList();
        ArrayList<Double> valuesR4 = new ArrayList();
        ArrayList<Double> valuesR5 = new ArrayList();
        ArrayList<Double> valuesR6 = new ArrayList();
        
        HashMap<String, ArrayList<Double>> measurements = new HashMap<>();
            // create object of JFileChooser, by default opens --> My Documents directory
            // JFileChooser chooser = new JFileChooser(); 
            // if we want to oped specific derictory
            // we should define directory path in JFileChooser parameters
            // For example I want to open folder with music
        JFileChooser chooser = new JFileChooser();
            // File Filter, we declare what format files we want to see
        FileFilter filter = new FileNameExtensionFilter("Text Files", "txt");
            // Apply Filter to types of files we want to see via FileChooser
        chooser.setFileFilter(filter);
        
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File fileToRead = chooser.getSelectedFile();// get file selected by user

            filePath = fileToRead.getAbsolutePath(); // get path to file

            try {
                // Create Scanner and Add selected file into the Scannner
                Scanner Obama = new Scanner(new File(filePath)); 

                while (Obama.hasNextLine()) { // Work with file, while file has more lines...
                    
                    String timeStamp = Obama.next(); // get date and time timestamp
                    timeStamps.add(timeStamp);       // add timeStamp into ArrayList --> used later on for creating Time line chart
                  
                    // LEFT FOOT SENSORS
                    double l1 = Double.parseDouble(Obama.next()); // get next string from scanner -> convert in into double
                    valuesL1.add(l1);                             // add double value to arraylist  
                    double l2 = Double.parseDouble(Obama.next());
                    valuesL2.add(l2);
                    double l3 = Double.parseDouble(Obama.next());
                    valuesL3.add(l3);
                    double l4 = Double.parseDouble(Obama.next());
                    valuesL4.add(l4);
                    double l5 = Double.parseDouble(Obama.next());
                    valuesL5.add(l5);
                    double l6 = Double.parseDouble(Obama.next());
                    valuesL6.add(l6);
                    
                    // RIGHT FOOT SENSORS
                    double r1 = Double.parseDouble(Obama.next());
                    valuesR1.add(r1);
                    double r2 = Double.parseDouble(Obama.next());
                    valuesR2.add(r2);
                    double r3 = Double.parseDouble(Obama.next());
                    valuesR3.add(r3);
                    double r4 = Double.parseDouble(Obama.next());
                    valuesR4.add(r4);
                    double r5 = Double.parseDouble(Obama.next());
                    valuesR5.add(r5);
                    double r6 = Double.parseDouble(Obama.next());
                    valuesR6.add(r6);
                } 
                // Add measurements in the arraylist as values to the hashmap 
                measurements.put("L1", valuesL1);
                measurements.put("L2", valuesL2);
                measurements.put("L3", valuesL3);
                measurements.put("L4", valuesL4);
                measurements.put("L5", valuesL5);
                measurements.put("L6", valuesL6);
                measurements.put("R1", valuesR1);
                measurements.put("R2", valuesR2);
                measurements.put("R3", valuesR3);
                measurements.put("R4", valuesR4);
                measurements.put("R5", valuesR5);
                measurements.put("R6", valuesR6);
            } catch (IOException ex) {
                Logger.getLogger(MainGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return measurements;
    }
   
    public static HashMap<String, ArrayList<Double>> ImportDataExistingData(String name, 
            String existingTimeStamp) throws Exception {
            
                ArrayList<Double> valuesL1 = new ArrayList();
                ArrayList<Double> valuesL2 = new ArrayList();
                ArrayList<Double> valuesL3 = new ArrayList();
                ArrayList<Double> valuesL4 = new ArrayList();
                ArrayList<Double> valuesL5 = new ArrayList();
                ArrayList<Double> valuesL6 = new ArrayList();
                ArrayList<Double> valuesR1 = new ArrayList();
                ArrayList<Double> valuesR2 = new ArrayList();
                ArrayList<Double> valuesR3 = new ArrayList();
                ArrayList<Double> valuesR4 = new ArrayList();
                ArrayList<Double> valuesR5 = new ArrayList();
                ArrayList<Double> valuesR6 = new ArrayList();
        
            String file = DBActions.getCLOB(name, existingTimeStamp);

            Scanner Obama;

            HashMap<String, ArrayList<Double>> measurements = new HashMap<>();
            
            Obama = new Scanner(file);
            while (Obama.hasNextLine()) {
                
                String timeStamp = Obama.next(); // skip date and time
                timeStamps.add(timeStamp);       // add timeStamp into ArrayList --> used lateron for creating Time line chart
                
                // LEFT FOOT SENSORS
                double l1 = Double.parseDouble(Obama.next()); // get next string from scanner. make it a double
                valuesL1.add(l1);                             // add double value to arraylist
                double l2 = Double.parseDouble(Obama.next());
                valuesL2.add(l2);
                double l3 = Double.parseDouble(Obama.next());
                valuesL3.add(l3);
                double l4 = Double.parseDouble(Obama.next());
                valuesL4.add(l4);
                double l5 = Double.parseDouble(Obama.next());
                valuesL5.add(l5);
                double l6 = Double.parseDouble(Obama.next());
                valuesL6.add(l6);
                
                // RIGHT FOOT SENSORS
                double r1 = Double.parseDouble(Obama.next());
                valuesR1.add(r1);
                double r2 = Double.parseDouble(Obama.next());
                valuesR2.add(r2);
                double r3 = Double.parseDouble(Obama.next());
                valuesR3.add(r3);
                double r4 = Double.parseDouble(Obama.next());
                valuesR4.add(r4);
                double r5 = Double.parseDouble(Obama.next());
                valuesR5.add(r5);
                double r6 = Double.parseDouble(Obama.next());
                valuesR6.add(r6);
            }
        measurements.put("L1", valuesL1);
        measurements.put("L2", valuesL2);
        measurements.put("L3", valuesL3);
        measurements.put("L4", valuesL4);
        measurements.put("L5", valuesL5);
        measurements.put("L6", valuesL6);
        measurements.put("R1", valuesR1);
        measurements.put("R2", valuesR2);
        measurements.put("R3", valuesR3);
        measurements.put("R4", valuesR4);
        measurements.put("R5", valuesR5);
        measurements.put("R6", valuesR6);
           
        return measurements;
    }
}