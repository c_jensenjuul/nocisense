package nocisense;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import static nocisense.TimeLineChart.splitedTimeStamp;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;

public class TimeLineChart extends ApplicationFrame {
    
    // Map containing measurements from txt. file or database. Example key = "L1 has value ArrayList<Integer> with al values from sensor L1
    private static HashMap<String, ArrayList<Double>> measurements = new HashMap(); 
    // Collection of all timestamps from txt. file or database.
    private static ArrayList<String> timeStamps = new ArrayList<>();  
    private static String sensor; // We need to specify, for which Sensor we want to create chart. 
    private static double thresholdX; // HighCount threshold shown on chart
    private static ChartPanel chartPanel; // Panel for holding in chart

    public static ChartPanel getChartPanel() {
        return chartPanel;
    }

    public TimeLineChart(double threshold, String sensor, HashMap<String, 
            ArrayList<Double>> measurementsX, ArrayList<String> timeStampsX) {

    super("measurements from " + sensor + " sensor"); // Create title for chart
    this.measurements = measurementsX; // measurements
    this.timeStamps = timeStampsX; // timestamps
    this.sensor = sensor; // selected foot zone
    this.thresholdX = threshold; // entered by user High threshold
    // Add data to chart, timestamps and values from sensor:
    // 14:00:00 - L1= 65.0, 14:00:01 - L1= 75.0, ....and so on
    XYDataset dataset = createDataset(); 
    JFreeChart chart = createChart(dataset); 

    XYPlot xyPlot = (XYPlot) chart.getPlot();
    NumberAxis range = (NumberAxis) xyPlot.getRangeAxis();
    range.setRange(0.0, 90.0); // Define Y - coordinate

    chartPanel = new ChartPanel(chart);
    chartPanel.setPreferredSize(new Dimension(740, 610));
    chartPanel.setMouseWheelEnabled(true);
    chartPanel.setMouseZoomable(true);
    chartPanel.setBackground(Color.BLACK);
    setContentPane(chartPanel);

    }

    /**
     * Creates a sample dataset.
     *
     * @return A sample dataset.
     */
    private XYDataset createDataset() {
        
        // Map containing measurements from txt. file or database
        HashMap<String, ArrayList<Double>> measurementsX = measurements;
        ArrayList<String> timeStampsX = timeStamps;// Collection of all timestamps from txt. file or database. 
        final TimeSeries sensor1 = new TimeSeries(sensor, Millisecond.class); // Line on chart that will show values from sensor, for example "L1"
        final TimeSeries threshold = new TimeSeries("Threshold"); // Threshold = equal value entered by user in MainGui window in High Threshold textfield
        ArrayList<Double> valuesFromSensor = new ArrayList<>(measurements.get(sensor));
        int times = timeStampsX.size() - 10; // how many time we need to run through the loop. - 10 because we using "10 step average system" otherwise we'll get OutOfBounds exception. So we actually not showing 1 last second from data file, what is not so critical and not influence on overall overview picture

        for (int i = 0; i <= times; i++) {

            String timeStamp = timeStampsX.get(i); // get timestamp from arraylust, For example 28-04-15/14:00:00:000

            ArrayList<Integer> splitedTimeStamp = splitedTimeStamp(timeStamp); // cut timestamp into small pieces, millisecond, second, minute.... etc..

            int millisecond = 100 * splitedTimeStamp.get(0); // get millisecond from timestamp
            int second = splitedTimeStamp.get(1); // getsecond from timestamp
            int minute = splitedTimeStamp.get(2); // get minute from timestamp
            int hour = splitedTimeStamp.get(3); // get hour from timestamp
            int day = splitedTimeStamp.get(4); // get day from timestamp
            int month = splitedTimeStamp.get(5); // get month from timestamp
            int year = 2000 + splitedTimeStamp.get(6); // get year from timestamp
            double sum = 0;
            
            //calculate 10 steps average. We take value associated with given timestamp + next 9 values   
            for (int j = 0; j < 10; j++) {
                sum = sum + valuesFromSensor.get(i + j);
            }
            // divide sum / 10
            double average = sum / 10;
            
            // Assign received average value to given timestamp
            sensor1.add(new Millisecond(millisecond, second, minute, hour, day, month, year), average); // add element ti timeseries dataset used for cheating 
            // HighCount threshold shown on chart, defined by user in MainGui window in High Threshold textfield
            threshold.add(new Millisecond(millisecond, second, minute, hour, day, month, year), thresholdX);
        }
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        dataset.addSeries(threshold);
        dataset.addSeries(sensor1);
        return dataset;
    }

    private JFreeChart createChart(final XYDataset dataset) {
        final JFreeChart chart = ChartFactory.createTimeSeriesChart(
                sensor,
                "Time",
                "Pressure",
                dataset,
                true, // include legend
                false, // tooltips?
                false
        );
        chart.setBackgroundPaint(new Color(214, 217, 223));
        final XYPlot plot = chart.getXYPlot();
        //plot.setOutlinePaint(null);
        plot.setBackgroundPaint(new Color(240, 242, 246));
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        return chart;
    }

    public static ArrayList<Integer> splitedTimeStamp(String dateAndTime) {
        ArrayList<Integer> splitedTimeStamp = new ArrayList<>();
        int millisecond = Integer.parseInt(dateAndTime.substring(18, 19));// Extract MILLISECOND from String and convert it into Int
        splitedTimeStamp.add(millisecond);
        int second = Integer.parseInt(dateAndTime.substring(15, 17));// Extract SECOND from String and convert it into Int
        splitedTimeStamp.add(second);
        int minute = Integer.parseInt(dateAndTime.substring(12, 14));// Extract MINUTE from String and convert it into Int
        splitedTimeStamp.add(minute);
        int hour = Integer.parseInt(dateAndTime.substring(9, 11));// Extract HOUR from String and convert it into Int
        splitedTimeStamp.add(hour);
        int day = Integer.parseInt(dateAndTime.substring(0, 2));  // Extract DAY from String and convert it into Int
        splitedTimeStamp.add(day);
        int month = Integer.parseInt(dateAndTime.substring(3, 5));  // Extract MONTH from String and convert it into Int
        splitedTimeStamp.add(month);
        int year = Integer.parseInt(dateAndTime.substring(6, 8)); // Extract YEAR from String and convert it into Int
        splitedTimeStamp.add(year);
        return splitedTimeStamp;
    }
       @Override
    public void windowClosing(WindowEvent event) {
        if(event.getWindow()==this){
            dispose();
        }
    }
}
