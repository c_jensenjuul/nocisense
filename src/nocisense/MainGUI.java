package nocisense;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RefineryUtilities;

/**
 *
 * @author Nikolai
 */
public class MainGUI extends javax.swing.JFrame {

    public static TimeSeriesCollection dataset;
    public static TimeSeriesCollection dataset2;
    public static HashMap<String, ArrayList<Double>> mapData = new HashMap<>();
    public static ArrayList<String> timeStamps = new ArrayList<>();
    public static HashMap<String, ArrayList<Integer>> mapCount = new HashMap<>();;
    public static String filePath;
    public static int counter;
    public static BarChart bc;
    public static boolean zonesToSelect = false;
    public static HashMap<String, ArrayList<String>> patientsAndMeasurements;
    public static String sensorForChart;
    public static ArrayList<String> patientNames = new ArrayList<>();

    /*
     mapCount
     patientsAndMeasurements
     mapCount
     */
    /**
     * Creates new form Application
     */
    public MainGUI() throws InterruptedException {
        this.setIconImage(new ImageIcon(getClass().getResource("Pictures/Logo_NociSense_S_uden tekst.png")).getImage());
        initComponents();
        createReportButton.setVisible(false);
        checkMarkFileLoaded.setVisible(false);
        loadNamesToCombobox();
        BarChart emptyChart = new BarChart("Measurements");
        ChartPanel.removeAll();
        ChartPanel.add(emptyChart.getChartPAnel(), BorderLayout.CENTER);
        ChartPanel.validate();

        displayHealthConditionBottom.setOpaque(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ChoosePatient = new javax.swing.JLabel();
        PatientsChooser = new javax.swing.JComboBox();
        ShowPatientsProfile = new javax.swing.JButton();
        MeasurementsHistory = new javax.swing.JLabel();
        MeasurementsChooser = new javax.swing.JComboBox();
        L1Text = new javax.swing.JLabel();
        L2Text = new javax.swing.JLabel();
        L3Text = new javax.swing.JLabel();
        L4Text = new javax.swing.JLabel();
        L5Text = new javax.swing.JLabel();
        L6Text = new javax.swing.JLabel();
        R1Text = new javax.swing.JLabel();
        R2Text = new javax.swing.JLabel();
        R3Text = new javax.swing.JLabel();
        R4Text = new javax.swing.JLabel();
        R5Text = new javax.swing.JLabel();
        R6Text = new javax.swing.JLabel();
        L1selected = new javax.swing.JLabel();
        L2selected = new javax.swing.JLabel();
        L3selected = new javax.swing.JLabel();
        L4selected = new javax.swing.JLabel();
        L5selected = new javax.swing.JLabel();
        L6selected = new javax.swing.JLabel();
        R1selected = new javax.swing.JLabel();
        R2selected = new javax.swing.JLabel();
        R3selected = new javax.swing.JLabel();
        R4selected = new javax.swing.JLabel();
        R5selected = new javax.swing.JLabel();
        R6selected = new javax.swing.JLabel();
        L1 = new javax.swing.JLabel();
        L2 = new javax.swing.JLabel();
        L3 = new javax.swing.JLabel();
        L4 = new javax.swing.JLabel();
        L5 = new javax.swing.JLabel();
        L6 = new javax.swing.JLabel();
        R1 = new javax.swing.JLabel();
        R2 = new javax.swing.JLabel();
        R3 = new javax.swing.JLabel();
        R4 = new javax.swing.JLabel();
        R5 = new javax.swing.JLabel();
        R6 = new javax.swing.JLabel();
        checkMarkFileLoaded = new javax.swing.JLabel();
        displayCprBottom = new javax.swing.JLabel();
        displayHealthConditionBottom = new javax.swing.JTextPane();
        newFileButton = new javax.swing.JButton();
        saveIntoDatabaseButton = new javax.swing.JButton();
        counterLabel = new javax.swing.JLabel();
        enterCounter = new javax.swing.JTextField();
        lowThreshold = new javax.swing.JLabel();
        enterLowThreshold = new javax.swing.JTextField();
        highThreshold = new javax.swing.JLabel();
        enterHighThreshold = new javax.swing.JTextField();
        applyThresholds = new javax.swing.JButton();
        createReportButton = new javax.swing.JButton();
        showChartForSelectedFootZone = new javax.swing.JButton();
        nocisenseLogoLabel = new javax.swing.JLabel();
        ChartPanel = new javax.swing.JPanel();
        measurementsInfoLabel = new javax.swing.JLabel();
        backGroundLabel = new javax.swing.JLabel();
        TopBarMenu = new javax.swing.JMenuBar();
        Patient = new javax.swing.JMenu();
        AddNewPatient = new javax.swing.JMenuItem();
        ClearWindow = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("NociSense");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ChoosePatient.setText("Choose patient");
        getContentPane().add(ChoosePatient, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        PatientsChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PatientsChooserActionPerformed(evt);
            }
        });
        getContentPane().add(PatientsChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 150, 30));

        ShowPatientsProfile.setText("?");
        ShowPatientsProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ShowPatientsProfileActionPerformed(evt);
            }
        });
        getContentPane().add(ShowPatientsProfile, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, 30));

        MeasurementsHistory.setText("Measurements Log");
        getContentPane().add(MeasurementsHistory, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 20, -1, -1));

        MeasurementsChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MeasurementsChooserActionPerformed(evt);
            }
        });
        getContentPane().add(MeasurementsChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 40, 170, 30));

        L1Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(L1Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 160, 90, 40));

        L2Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(L2Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(97, 205, 50, 40));

        L3Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(L3Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 260, 90, 40));

        L4Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(L4Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(94, 312, 90, 40));

        L5Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(L5Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 370, 80, 40));

        L6Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(L6Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(97, 410, 90, 40));

        R1Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(R1Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(273, 165, 60, 40));

        R2Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(R2Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(295, 207, 50, 40));

        R3Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(R3Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 260, 90, 40));

        R4Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(R4Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 320, 90, 30));

        R5Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(R5Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(97, 365, 80, 40));

        R6Text.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        getContentPane().add(R6Text, new org.netbeans.lib.awtextra.AbsoluteConstraints(297, 415, 90, 40));

        L1selected.setVerifyInputWhenFocusTarget(false);
        getContentPane().add(L1selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(28, 92, 160, 120));
        getContentPane().add(L2selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 190, 170, 80));
        getContentPane().add(L3selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(29, 240, 170, 80));
        getContentPane().add(L4selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(35, 290, 170, 80));
        getContentPane().add(L5selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(42, 345, 170, 80));
        getContentPane().add(L6selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 397, 170, 80));

        R1selected.setVerifyInputWhenFocusTarget(false);
        getContentPane().add(R1selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(225, 92, 160, 120));
        getContentPane().add(R2selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(222, 190, 170, 80));
        getContentPane().add(R3selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(228, 240, 170, 80));
        getContentPane().add(R4selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(252, 295, 170, 80));
        getContentPane().add(R5selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(245, 348, 170, 80));
        getContentPane().add(R6selected, new org.netbeans.lib.awtextra.AbsoluteConstraints(246, 397, 170, 80));

        L1.setBackground(new java.awt.Color(255, 255, 255));
        L1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/L1-lightGray.png"))); // NOI18N
        L1.setToolTipText("");
        L1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                L1MouseClicked(evt);
            }
        });
        getContentPane().add(L1, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 104, 170, -1));

        L2.setBackground(new java.awt.Color(255, 255, 255));
        L2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/L2-lightGray.png"))); // NOI18N
        L2.setToolTipText("");
        L2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                L2MouseClicked(evt);
            }
        });
        getContentPane().add(L2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 196, 170, 58));

        L3.setBackground(new java.awt.Color(255, 255, 255));
        L3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/L3-lightGray.png"))); // NOI18N
        L3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                L3MouseClicked(evt);
            }
        });
        getContentPane().add(L3, new org.netbeans.lib.awtextra.AbsoluteConstraints(31, 250, 160, 57));

        L4.setBackground(new java.awt.Color(255, 255, 255));
        L4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/L4-lightGray.png"))); // NOI18N
        L4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                L4MouseClicked(evt);
            }
        });
        getContentPane().add(L4, new org.netbeans.lib.awtextra.AbsoluteConstraints(28, 302, 150, 60));

        L5.setBackground(new java.awt.Color(255, 255, 255));
        L5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/L5-lightGray.png"))); // NOI18N
        L5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                L5MouseClicked(evt);
            }
        });
        getContentPane().add(L5, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 358, 170, 56));

        L6.setBackground(new java.awt.Color(255, 255, 255));
        L6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/L6-lightGray.png"))); // NOI18N
        L6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                L6MouseClicked(evt);
            }
        });
        getContentPane().add(L6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 409, 160, 55));

        R1.setBackground(new java.awt.Color(102, 255, 102));
        R1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        R1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/R1-lightGray.png"))); // NOI18N
        R1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                R1MouseClicked(evt);
            }
        });
        getContentPane().add(R1, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 102, 160, 100));

        R2.setBackground(new java.awt.Color(102, 255, 102));
        R2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        R2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/R2-lightGray.png"))); // NOI18N
        R2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                R2MouseClicked(evt);
            }
        });
        getContentPane().add(R2, new org.netbeans.lib.awtextra.AbsoluteConstraints(229, 196, 160, 60));

        R3.setBackground(new java.awt.Color(102, 255, 102));
        R3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        R3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/R3-lightGray.png"))); // NOI18N
        R3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                R3MouseClicked(evt);
            }
        });
        getContentPane().add(R3, new org.netbeans.lib.awtextra.AbsoluteConstraints(232, 249, 160, 60));

        R4.setBackground(new java.awt.Color(102, 255, 102));
        R4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        R4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/R4-lightGray.png"))); // NOI18N
        R4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                R4MouseClicked(evt);
            }
        });
        getContentPane().add(R4, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 304, 160, 60));

        R5.setBackground(new java.awt.Color(102, 255, 102));
        R5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        R5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/R5-lightGray.png"))); // NOI18N
        R5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                R5MouseClicked(evt);
            }
        });
        getContentPane().add(R5, new org.netbeans.lib.awtextra.AbsoluteConstraints(234, 358, 160, 60));

        R6.setBackground(new java.awt.Color(102, 255, 102));
        R6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        R6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/R6-lightGray.png"))); // NOI18N
        R6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                R6MouseClicked(evt);
            }
        });
        getContentPane().add(R6, new org.netbeans.lib.awtextra.AbsoluteConstraints(233, 408, 160, 60));

        checkMarkFileLoaded.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/icon_checkmark_green_small.png"))); // NOI18N
        checkMarkFileLoaded.setToolTipText("");
        getContentPane().add(checkMarkFileLoaded, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 630, -1, -1));
        getContentPane().add(displayCprBottom, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 530, 520, 30));

        displayHealthConditionBottom.setEditable(false);
        displayHealthConditionBottom.setBackground(new java.awt.Color(242, 238, 238));
        displayHealthConditionBottom.setBorder(null);
        displayHealthConditionBottom.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        displayHealthConditionBottom.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        displayHealthConditionBottom.setEnabled(false);
        displayHealthConditionBottom.setFocusCycleRoot(false);
        displayHealthConditionBottom.setFocusable(false);
        displayHealthConditionBottom.setOpaque(false);
        displayHealthConditionBottom.setRequestFocusEnabled(false);
        getContentPane().add(displayHealthConditionBottom, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 560, 520, 60));

        newFileButton.setText("New File");
        newFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newFileButtonActionPerformed(evt);
            }
        });
        getContentPane().add(newFileButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 80, -1, 30));

        saveIntoDatabaseButton.setText("Save measurements");
        saveIntoDatabaseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveIntoDatabaseButtonActionPerformed(evt);
            }
        });
        getContentPane().add(saveIntoDatabaseButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 40, 150, 30));

        counterLabel.setText("Counter");
        getContentPane().add(counterLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 200, -1, -1));
        getContentPane().add(enterCounter, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 220, 130, -1));

        lowThreshold.setText("Low Threshold");
        getContentPane().add(lowThreshold, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 250, -1, -1));
        getContentPane().add(enterLowThreshold, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 270, 130, -1));

        highThreshold.setText("High Threshold");
        getContentPane().add(highThreshold, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 300, -1, -1));
        getContentPane().add(enterHighThreshold, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 320, 130, -1));

        applyThresholds.setText("Apply Thresholds");
        applyThresholds.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyThresholdsActionPerformed(evt);
            }
        });
        getContentPane().add(applyThresholds, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 350, -1, -1));

        createReportButton.setText("Create Report");
        createReportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createReportButtonActionPerformed(evt);
            }
        });
        getContentPane().add(createReportButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 390, 130, -1));

        showChartForSelectedFootZone.setText("OpenChart");
        showChartForSelectedFootZone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showChartForSelectedFootZoneActionPerformed(evt);
            }
        });
        getContentPane().add(showChartForSelectedFootZone, new org.netbeans.lib.awtextra.AbsoluteConstraints(1220, 620, -1, -1));

        nocisenseLogoLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        nocisenseLogoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nocisense/Pictures/logo40percent.png"))); // NOI18N
        getContentPane().add(nocisenseLogoLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 30, 480, 520));

        ChartPanel.setLayout(new java.awt.BorderLayout());
        getContentPane().add(ChartPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 10, 740, 610));
        getContentPane().add(measurementsInfoLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 625, 540, 30));

        backGroundLabel.setBackground(null
        );
        backGroundLabel.setOpaque(true);
        backGroundLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backGroundLabelMouseClicked(evt);
            }
        });
        getContentPane().add(backGroundLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1340, 670));

        Patient.setText("Patient");

        AddNewPatient.setText("Add new");
        AddNewPatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddNewPatientActionPerformed(evt);
            }
        });
        Patient.add(AddNewPatient);

        TopBarMenu.add(Patient);

        ClearWindow.setText("Clear Window");
        ClearWindow.setActionCommand("Clear window");
        ClearWindow.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ClearWindowMouseClicked(evt);
            }
        });
        TopBarMenu.add(ClearWindow);

        setJMenuBar(TopBarMenu);

        setSize(new java.awt.Dimension(1351, 733));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void ShowPatientsProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ShowPatientsProfileActionPerformed
        String patient = PatientsChooser.getSelectedItem().toString();
        if (!patient.equals("")) {
            new PatientWindow(patient).setVisible(true);
        }
    }//GEN-LAST:event_ShowPatientsProfileActionPerformed

    private void newFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newFileButtonActionPerformed
        timeStamps.clear(); // Clear ArrayList holding timestamps 
        mapData.clear();    // Clear HashMap holding measurements for all sensors
        mapData = ImportNewData.ImportDataFromFile(); // import new measurements from selected file
        timeStamps = ImportNewData.getTimeStamps();   // import timestamps  from selected file
        if (!mapData.isEmpty()) { // if we have measurements for analysis, we can start doing computations

        // extract date and time from file's 1-st line timestamp // for example 28.05.2015 15:00
        String timeStampStart = timeStamps.get(0).substring(0, 8) + " " + timeStamps.get(0).substring(9, 14);
        // extract time (hour and minutes) from file's last line timestamp // 14:00
        String timeStampEnd = timeStamps.get(timeStamps.size() - 1).substring(9, 14); 

        PatientsChooser.setSelectedItem(0); // unselect patient who is currently shown in combo box

        // Show to the user, what file with measurements he/she has selected
        measurementsInfoLabel.setText("File is selected: measurements from " + timeStampStart + "-" + timeStampEnd); 
        // make visible icon,that is shown to the user only if file is selected and measurements are imported and ready for analysis
        checkMarkFileLoaded.setVisible(true); 
        }
    }//GEN-LAST:event_newFileButtonActionPerformed

    private void PatientsChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PatientsChooserActionPerformed
        String patientName = PatientsChooser.getSelectedItem().toString();
        Object[] measurements = patientsAndMeasurements.get(patientName).toArray();
        MeasurementsChooser.setModel(new DefaultComboBoxModel(measurements));
        if (!patientName.equals("")) {
            Patient pChosen = DBActions.loadPatientInfo(patientName);
            displayCprBottom.setText("CPR:    " + pChosen.getCpr());
            String HealthCondition = "";
            String[] healthCondition = pChosen.getHealthCondition().split("/n");
            displayHealthConditionBottom.setText("Health condition: \n" + pChosen.getHealthCondition());
            measurementsInfoLabel.setText("");
            checkMarkFileLoaded.setVisible(false);

        } else {
            displayCprBottom.setText("");
            displayHealthConditionBottom.setText("");
        }
        clearWindow();

    }//GEN-LAST:event_PatientsChooserActionPerformed

    private void AddNewPatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddNewPatientActionPerformed
        new PatientWindow().setVisible(true);
    }//GEN-LAST:event_AddNewPatientActionPerformed

    private void applyThresholdsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_applyThresholdsActionPerformed
        if (!mapData.isEmpty()) { // if we have measurements for analysis
            String stringCounter = enterCounter.getText(); // get Counter entered by user
            String stringLowThreshold = enterLowThreshold.getText(); // get Low Threshold  entered by user
            String stringHighThreshold = enterHighThreshold.getText();// get High threshold  entered by user

            if (InputChecker.integerChecker(stringCounter)
                    && InputChecker.integerChecker(stringHighThreshold)
                    && InputChecker.integerChecker(stringLowThreshold)) {
                //Check for input validity Counter,HighThreshold,LowThreshold

                counter = Integer.parseInt(stringCounter);// convert String into int
                int lowThreshold = Integer.parseInt(enterLowThreshold.getText());
                int highThreshold = Integer.parseInt(enterHighThreshold.getText());
                if(mapCount!=null)// if HashMap contains old data...
                {
                    mapCount = null; // clear HashMap
                }
               
                mapCount = Analyzer.getCounts(mapData, lowThreshold, highThreshold);// get Analysed results
                displayZonesWithCounts(mapCount, counter);// color feet based on analysis results
                clearSelectedZones();// remove selection from feet zones
                this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR)); // SHOW LOADING PROCESS

                //BARCHART
                bc = new BarChart("Measurements", mapCount, counter);//create Bar cahrt
                ChartPanel.removeAll();
                ChartPanel.add(bc.getChartPAnel(), BorderLayout.CENTER); // add barchart to mainGUi Panel
                ChartPanel.validate();

                this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));// SHOW THAT PROCESS IS FINISHED

                zonesToSelect = true;
                createReportButton.setVisible(true);

                nocisenseLogoLabel.setVisible(false);
            } else {
                JOptionPane.showOptionDialog(null, "Input should be whole number.\nNo space allowed.", "Invalid Input", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, new Object[]{}, null);
            }
        } else {
            JOptionPane.showOptionDialog(null, "Import data", "No data improted", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, new Object[]{}, null);
        }
    }//GEN-LAST:event_applyThresholdsActionPerformed

    private void ClearWindowMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ClearWindowMouseClicked
        clearWindow();
        clearWindowOfPatient();
    }//GEN-LAST:event_ClearWindowMouseClicked

    private void L1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_L1MouseClicked
        if (zonesToSelect) {// if we have analyzed data for creating char
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();// remove selection from previous selected zone
            // highlight selected zone
            L1selected.setIcon(new ImageIcon(getClass().getResource("Pictures/L1-selected.png"))); 
            showTimeLineChart("L1"); //creare and show chart
            sensorForChart = "L1";   //save selected sensor's name as static String
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_L1MouseClicked

    private void L2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_L2MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            L2selected.setIcon(new ImageIcon(getClass().getResource("Pictures/L2-selected.png")));
            showTimeLineChart("L2");
            sensorForChart = "L2";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_L2MouseClicked

    private void L3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_L3MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            L3selected.setIcon(new ImageIcon(getClass().getResource("Pictures/L3-selected.png")));
            showTimeLineChart("L3");
            sensorForChart = "L3";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_L3MouseClicked

    private void L4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_L4MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            L4selected.setIcon(new ImageIcon(getClass().getResource("Pictures/L4-selected.png")));
            showTimeLineChart("L4");
            sensorForChart = "L4";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_L4MouseClicked

    private void L5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_L5MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            L5selected.setIcon(new ImageIcon(getClass().getResource("Pictures/L5-selected.png")));
            showTimeLineChart("L5");
            sensorForChart = "L5";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_L5MouseClicked

    private void L6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_L6MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            L6selected.setIcon(new ImageIcon(getClass().getResource("Pictures/L6-selected.png")));
            showTimeLineChart("L6");
            sensorForChart = "L6";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_L6MouseClicked

    private void R1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_R1MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            R1selected.setIcon(new ImageIcon(getClass().getResource("Pictures/R1-selected.png")));
            showTimeLineChart("R1");
            sensorForChart = "R1";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        }
    }//GEN-LAST:event_R1MouseClicked

    private void R2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_R2MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            R2selected.setIcon(new ImageIcon(getClass().getResource("Pictures/R2-selected.png")));
            showTimeLineChart("R2");
            sensorForChart = "R2";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_R2MouseClicked

    private void R3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_R3MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            R3selected.setIcon(new ImageIcon(getClass().getResource("Pictures/R3-selected.png")));
            showTimeLineChart("R3");
            sensorForChart = "R3";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_R3MouseClicked

    private void R4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_R4MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            R4selected.setIcon(new ImageIcon(getClass().getResource("Pictures/R4-selected.png")));
            showTimeLineChart("R4");
            sensorForChart = "R4";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_R4MouseClicked

    private void R5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_R5MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            R5selected.setIcon(new ImageIcon(getClass().getResource("Pictures/R5-selected.png")));
            showTimeLineChart("R5");
            sensorForChart = "R5";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_R5MouseClicked

    private void R6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_R6MouseClicked
        if (zonesToSelect) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            clearSelectedZones();
            R6selected.setIcon(new ImageIcon(getClass().getResource("Pictures/R6-selected.png")));
            showTimeLineChart("R6");
            sensorForChart = "R6";
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_R6MouseClicked

    private void MeasurementsChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MeasurementsChooserActionPerformed
        String patientName = PatientsChooser.getSelectedItem().toString();
        String timeStamp = MeasurementsChooser.getSelectedItem().toString();

        mapData.clear();
        timeStamps.clear();
        clearWindow();

        if (timeStamp.length() != 0) {
            try {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                mapData = ImportNewData.ImportDataExistingData(patientName, timeStamp);
                timeStamps = ImportNewData.getTimeStamps();

                String timeStampStart = timeStamps.get(0).substring(0, 8) + " " + timeStamps.get(0).substring(9, 14);
                String timeStampEnd = timeStamps.get(timeStamps.size() - 1).substring(9, 14);

                measurementsInfoLabel.setText("File is selected: measurements from " + timeStampStart + "-" + timeStampEnd);
                checkMarkFileLoaded.setVisible(true);
                this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

                // PLACE WHERE TO SAVE FILE INTO DB
            } catch (Exception ex) {
                Logger.getLogger(MainGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


    }//GEN-LAST:event_MeasurementsChooserActionPerformed

    private void backGroundLabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backGroundLabelMouseClicked
        if (zonesToSelect) {
            clearSelectedZones();

            if (bc != null) {
                bc = new BarChart("Measurements", mapCount, counter);
                ChartPanel.removeAll();
                ChartPanel.add(bc.getChartPAnel(), BorderLayout.CENTER);
                ChartPanel.validate();
            }
        }
    }//GEN-LAST:event_backGroundLabelMouseClicked

    private void saveIntoDatabaseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveIntoDatabaseButtonActionPerformed

        String patient = PatientsChooser.getSelectedItem().toString(); // Get patients's name from combo box
        if (!mapData.isEmpty()) {
            // extract date and time from file's 1-st line timestamp // for example 28.05.2015 15:00
            String timeStampStart = timeStamps.get(0).substring(0, 8) + " " + timeStamps.get(0).substring(9, 14); 
            // extract time from file's last line timestamp
            String timeStampEnd = timeStamps.get(timeStamps.size() - 1).substring(9, 14); 
            // test database content, if this particular user already have 
            if (!DBActions.getMeasurementsHistory(patient).contains(timeStampStart + "-" + timeStampEnd) && MeasurementsChooser.getSelectedIndex() <= 0) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                try {
                    DBActions.saveTextFileIntoDatabase(filePath, patient, timeStampStart + "-" + timeStampEnd);

                } catch (FileNotFoundException ex) {
                    System.out.println(ex.getMessage());
                }
                // Future improvements:
                // Inform user about successful operation ( File is saved into DB )

                String patientName = PatientsChooser.getSelectedItem().toString();
                Object[] measurements = DBActions.getMeasurementsHistory(patientName).toArray();
                MeasurementsChooser.setModel(new DefaultComboBoxModel(measurements));
                this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            } else {
                System.err.println("error saving to db");
                // Inform user about FAILED operation ( File is NOT saved into DB)
            }
        }
    }//GEN-LAST:event_saveIntoDatabaseButtonActionPerformed

    private void createReportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createReportButtonActionPerformed

        String timeStampStart = timeStamps.get(0).substring(0, 8) + " " + timeStamps.get(0).substring(9, 14);
        String timeStampEnd = timeStamps.get(timeStamps.size() - 1).substring(9, 14);
        String timeStamp = timeStampStart + "-" + timeStampEnd;

        Report.createPdfReport(PatientsChooser.getSelectedItem().toString(),
                timeStamp, enterCounter.getText(),
                enterLowThreshold.getText(), enterHighThreshold.getText(), mapCount);
    }//GEN-LAST:event_createReportButtonActionPerformed

    private void showChartForSelectedFootZoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showChartForSelectedFootZoneActionPerformed

        if (sensorForChart != null && mapData != null && timeStamps != null && enterHighThreshold.getText().length() > 0) {
            double threshold = Double.parseDouble(enterHighThreshold.getText());
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            TimeLineChart demo2 = new TimeLineChart(threshold, sensorForChart, mapData, timeStamps);
            demo2.pack();
            RefineryUtilities.centerFrameOnScreen(demo2);
            demo2.setVisible(true);
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        }
    }//GEN-LAST:event_showChartForSelectedFootZoneActionPerformed

    private void AboutMenuItemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AboutMenuItemMouseClicked

    }//GEN-LAST:event_AboutMenuItemMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new MainGUI().setVisible(true);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MainGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JMenuItem AddNewPatient;
    public static javax.swing.JPanel ChartPanel;
    public static javax.swing.JLabel ChoosePatient;
    public static javax.swing.JMenu ClearWindow;
    public static javax.swing.JLabel L1;
    public static javax.swing.JLabel L1Text;
    public static javax.swing.JLabel L1selected;
    public static javax.swing.JLabel L2;
    public static javax.swing.JLabel L2Text;
    public static javax.swing.JLabel L2selected;
    public static javax.swing.JLabel L3;
    public static javax.swing.JLabel L3Text;
    public static javax.swing.JLabel L3selected;
    public static javax.swing.JLabel L4;
    public static javax.swing.JLabel L4Text;
    public static javax.swing.JLabel L4selected;
    public static javax.swing.JLabel L5;
    public static javax.swing.JLabel L5Text;
    public static javax.swing.JLabel L5selected;
    public static javax.swing.JLabel L6;
    public static javax.swing.JLabel L6Text;
    public static javax.swing.JLabel L6selected;
    public static javax.swing.JComboBox MeasurementsChooser;
    public static javax.swing.JLabel MeasurementsHistory;
    public static javax.swing.JMenu Patient;
    public static javax.swing.JComboBox PatientsChooser;
    public static javax.swing.JLabel R1;
    public static javax.swing.JLabel R1Text;
    public static javax.swing.JLabel R1selected;
    public static javax.swing.JLabel R2;
    public static javax.swing.JLabel R2Text;
    public static javax.swing.JLabel R2selected;
    public static javax.swing.JLabel R3;
    public static javax.swing.JLabel R3Text;
    public static javax.swing.JLabel R3selected;
    public static javax.swing.JLabel R4;
    public static javax.swing.JLabel R4Text;
    public static javax.swing.JLabel R4selected;
    public static javax.swing.JLabel R5;
    public static javax.swing.JLabel R5Text;
    public static javax.swing.JLabel R5selected;
    public static javax.swing.JLabel R6;
    public static javax.swing.JLabel R6Text;
    public static javax.swing.JLabel R6selected;
    public static javax.swing.JButton ShowPatientsProfile;
    public static javax.swing.JMenuBar TopBarMenu;
    public static javax.swing.JButton applyThresholds;
    public static javax.swing.JLabel backGroundLabel;
    public static javax.swing.JLabel checkMarkFileLoaded;
    public static javax.swing.JLabel counterLabel;
    public static javax.swing.JButton createReportButton;
    public static javax.swing.JLabel displayCprBottom;
    public static javax.swing.JTextPane displayHealthConditionBottom;
    public static javax.swing.JTextField enterCounter;
    public static javax.swing.JTextField enterHighThreshold;
    public static javax.swing.JTextField enterLowThreshold;
    public static javax.swing.JLabel highThreshold;
    public static javax.swing.JLabel lowThreshold;
    public static javax.swing.JLabel measurementsInfoLabel;
    public static javax.swing.JButton newFileButton;
    public static javax.swing.JLabel nocisenseLogoLabel;
    public static javax.swing.JButton saveIntoDatabaseButton;
    public static javax.swing.JButton showChartForSelectedFootZone;
    // End of variables declaration//GEN-END:variables

    public void displayZonesWithCounts(HashMap<String, ArrayList<Integer>> mapCounts, int counter) {
    // Change color and text of each zone, one zone at the time

        // L1   
        // if at key L1, first value in ArrayList of values (highCount) is larger than counter
        if (mapCounts.get("L1").get(0) > counter) {
            L1.setIcon(new ImageIcon(getClass().getResource("Pictures/L1-red.png")));
            L1Text.setText(mapCounts.get("L1").get(0) + "");

            // if at key L1, second value in ArrayList of values (lowCount) is larger than counter
        } else if (mapCounts.get("L1").get(1) > counter) {
            L1.setIcon(new ImageIcon(getClass().getResource("Pictures/L1-yellow.png")));
            L1Text.setText(mapCounts.get("L1").get(1) + "");
        } else {
            L1.setIcon(new ImageIcon(getClass().getResource("Pictures/L1-green.png")));
            L1Text.setText(mapCounts.get("L1").get(1) + "");
        }
        // L2
        if (mapCounts.get("L2").get(0) > counter) {
            L2.setIcon(new ImageIcon(getClass().getResource("Pictures/L2-red.png")));
            L2Text.setText(mapCounts.get("L2").get(0) + "");
        } else if (mapCounts.get("L2").get(1) > counter) {
            L2.setIcon(new ImageIcon(getClass().getResource("Pictures/L2-yellow.png")));
            L2Text.setText(mapCounts.get("L2").get(1) + "");
        } else {
            L2.setIcon(new ImageIcon(getClass().getResource("Pictures/L2-green.png")));
            L2Text.setText(mapCounts.get("L2").get(1) + "");
        }
        // L3
        if (mapCounts.get("L3").get(0) > counter) {
            L3.setIcon(new ImageIcon(getClass().getResource("Pictures/L3-red.png")));
            L3Text.setText(mapCounts.get("L3").get(0) + "");
        } else if (mapCounts.get("L3").get(1) > counter) {
            L3.setIcon(new ImageIcon(getClass().getResource("Pictures/L3-yellow.png")));
            L3Text.setText(mapCounts.get("L3").get(1) + "");
        } else {
            L3.setIcon(new ImageIcon(getClass().getResource("Pictures/L3-green.png")));
            L3Text.setText(mapCounts.get("L3").get(1) + "");
        }
        // L4
        if (mapCounts.get("L4").get(0) > counter) {
            L4.setIcon(new ImageIcon(getClass().getResource("Pictures/L4-red.png")));
            L4Text.setText(mapCounts.get("L4").get(0) + "");
        } else if (mapCounts.get("L4").get(1) > counter) {
            L4.setIcon(new ImageIcon(getClass().getResource("Pictures/L4-yellow.png")));
            L4Text.setText(mapCounts.get("L4").get(1) + "");
        } else {
            L4.setIcon(new ImageIcon(getClass().getResource("Pictures/L4-green.png")));
            L4Text.setText(mapCounts.get("L4").get(1) + "");
        }
        // L5
        if (mapCounts.get("L5").get(0) > counter) {
            L5.setIcon(new ImageIcon(getClass().getResource("Pictures/L5-red.png")));
            L5Text.setText(mapCounts.get("L5").get(0) + "");
        } else if (mapCounts.get("L5").get(1) > counter) {
            L5.setIcon(new ImageIcon(getClass().getResource("Pictures/L5-yellow.png")));
            L5Text.setText(mapCounts.get("L5").get(1) + "");
        } else {
            L5.setIcon(new ImageIcon(getClass().getResource("Pictures/L5-green.png")));
            L5Text.setText(mapCounts.get("L5").get(1) + "");
        }
        // L6
        if (mapCounts.get("L6").get(0) > counter) {
            L6.setIcon(new ImageIcon(getClass().getResource("Pictures/L6-red.png")));
            L6Text.setText(mapCounts.get("L6").get(0) + "");
        } else if (mapCounts.get("L6").get(1) > counter) {
            L6.setIcon(new ImageIcon(getClass().getResource("Pictures/L6-yellow.png")));
            L6Text.setText(mapCounts.get("L6").get(1) + "");
        } else {
            L6.setIcon(new ImageIcon(getClass().getResource("Pictures/L6-green.png")));
            L6Text.setText(mapCounts.get("L6").get(1) + "");
        }
        // R1
        if (mapCounts.get("R1").get(0) > counter) {
            R1.setIcon(new ImageIcon(getClass().getResource("Pictures/R1-red.png")));
            R1Text.setText(mapCounts.get("R1").get(0) + "");
        } else if (mapCounts.get("R1").get(1) > counter) {
            R1.setIcon(new ImageIcon(getClass().getResource("Pictures/R1-yellow.png")));
            R1Text.setText(mapCounts.get("R1").get(1) + "");
        } else {
            R1.setIcon(new ImageIcon(getClass().getResource("Pictures/R1-green.png")));
            R1Text.setText(mapCounts.get("R1").get(1) + "");
        }
        // R2
        if (mapCounts.get("R2").get(0) > counter) {
            R2.setIcon(new ImageIcon(getClass().getResource("Pictures/R2-red.png")));
            R2Text.setText(mapCounts.get("R2").get(0) + "");
        } else if (mapCounts.get("R2").get(1) > counter) {
            R2.setIcon(new ImageIcon(getClass().getResource("Pictures/R2-yellow.png")));
            R2Text.setText(mapCounts.get("R2").get(1) + "");
        } else {
            R2.setIcon(new ImageIcon(getClass().getResource("Pictures/R2-green.png")));
            R2Text.setText(mapCounts.get("R2").get(1) + "");
        }
        // R3
        if (mapCounts.get("R3").get(0) > counter) {
            R3.setIcon(new ImageIcon(getClass().getResource("Pictures/R3-red.png")));
            R3Text.setText(mapCounts.get("R3").get(0) + "");
        } else if (mapCounts.get("R3").get(1) > counter) {
            R3.setIcon(new ImageIcon(getClass().getResource("Pictures/R3-yellow.png")));
            R3Text.setText(mapCounts.get("R3").get(1) + "");
        } else {
            R3.setIcon(new ImageIcon(getClass().getResource("Pictures/R3-green.png")));
            R3Text.setText(mapCounts.get("R3").get(1) + "");
        }
        // R4
        if (mapCounts.get("R4").get(0) > counter) {
            R4.setIcon(new ImageIcon(getClass().getResource("Pictures/R4-red.png")));
            R4Text.setText(mapCounts.get("R4").get(0) + "");
        } else if (mapCounts.get("R4").get(1) > counter) {
            R4.setIcon(new ImageIcon(getClass().getResource("Pictures/R4-yellow.png")));
            R4Text.setText(mapCounts.get("R4").get(1) + "");
        } else {
            R4.setIcon(new ImageIcon(getClass().getResource("Pictures/R4-green.png")));
            R4Text.setText(mapCounts.get("R4").get(1) + "");
        }
        // R5
        if (mapCounts.get("R5").get(0) > counter) {
            R5.setIcon(new ImageIcon(getClass().getResource("Pictures/R5-red.png")));
            R5Text.setText(mapCounts.get("R5").get(0) + "");
        } else if (mapCounts.get("R5").get(1) > counter) {
            R5.setIcon(new ImageIcon(getClass().getResource("Pictures/R5-yellow.png")));
            R5Text.setText(mapCounts.get("R5").get(1) + "");
        } else {
            R5.setIcon(new ImageIcon(getClass().getResource("Pictures/R5-green.png")));
            R5Text.setText(mapCounts.get("R5").get(1) + "");
        }
        // R6
        if (mapCounts.get("R6").get(0) > counter) {
            R6.setIcon(new ImageIcon(getClass().getResource("Pictures/R6-red.png")));
            R6Text.setText(mapCounts.get("R6").get(0) + "");
        } else if (mapCounts.get("R6").get(1) > counter) {
            R6.setIcon(new ImageIcon(getClass().getResource("Pictures/R6-yellow.png")));
            R6Text.setText(mapCounts.get("R6").get(1) + "");
        } else {
            R6.setIcon(new ImageIcon(getClass().getResource("Pictures/R6-green.png")));
            R6Text.setText(mapCounts.get("R6").get(1) + "");
        }
    }

    private void showTimeLineChart(String l1) {

        // if we have measurements for analysis
        if (mapData != null && timeStamps != null && InputChecker.integerChecker(enterHighThreshold.getText())) {
            double threshold = Double.parseDouble(enterHighThreshold.getText());
            //Create TimeLineChart
            TimeLineChart demo = new TimeLineChart(threshold, l1, mapData, timeStamps);

            ChartPanel.removeAll();
            //Add chartPanel with chart into MainGUI Panel
            ChartPanel.add(demo.getChartPanel(), BorderLayout.CENTER);
            ChartPanel.validate();

        }
    }

    private void clearSelectedZones() {
        
        L1selected.setIcon(null);
        L2selected.setIcon(null);
        L3selected.setIcon(null);
        L4selected.setIcon(null);
        L5selected.setIcon(null);
        L6selected.setIcon(null);

        R1selected.setIcon(null);
        R2selected.setIcon(null);
        R3selected.setIcon(null);
        R4selected.setIcon(null);
        R5selected.setIcon(null);
        R6selected.setIcon(null);
    }

    public static void loadNamesToCombobox() {

        patientsAndMeasurements = DBActions.getPatientsAndMeasurements();
        patientNames = new ArrayList(patientsAndMeasurements.keySet());
        Collections.sort(patientNames);
        Object[] patients = patientNames.toArray();
        PatientsChooser.setModel(new javax.swing.DefaultComboBoxModel(patients));
    }

    public static void deleteNameFromCombobox(String name) {

        patientsAndMeasurements.remove(name);
        patientNames.remove(name);
        Object[] patients = patientNames.toArray();
        PatientsChooser.setModel(new javax.swing.DefaultComboBoxModel(patients));
        clearWindowOfPatient();
    }

    public static void addNameToCombobox(String name) {

        patientsAndMeasurements.put(name, new ArrayList<String>());
        patientNames.add(name);
        Collections.sort(patientNames);
        Object[] patients = patientNames.toArray();
        PatientsChooser.setModel(new javax.swing.DefaultComboBoxModel(patients));

    }

    public static void updateNameToCombobox(String name, String oldName) {
        patientsAndMeasurements.put(name, patientsAndMeasurements.get(oldName));
        patientsAndMeasurements.remove(oldName);
        patientNames.remove(oldName);
        patientNames.add(name);
        Collections.sort(patientNames);
        Object[] patients = patientNames.toArray();
        PatientsChooser.setModel(new javax.swing.DefaultComboBoxModel(patients));

    }

    private void clearWindow() {
        // Clear text from labels and textfields
        
        enterCounter.setText("");
        enterHighThreshold.setText("");
        enterLowThreshold.setText("");
        L1Text.setText("");
        L2Text.setText("");
        L3Text.setText("");
        L4Text.setText("");
        L5Text.setText("");
        L6Text.setText("");
        R1Text.setText("");
        R2Text.setText("");
        R3Text.setText("");
        R4Text.setText("");
        R5Text.setText("");
        R6Text.setText("");
        // Reset zone labels to light gray fot
        L1.setIcon(new ImageIcon(getClass().getResource("Pictures/L1-lightGray.png")));
        L2.setIcon(new ImageIcon(getClass().getResource("Pictures/L2-lightGray.png")));
        L3.setIcon(new ImageIcon(getClass().getResource("Pictures/L3-lightGray.png")));
        L4.setIcon(new ImageIcon(getClass().getResource("Pictures/L4-lightGray.png")));
        L5.setIcon(new ImageIcon(getClass().getResource("Pictures/L5-lightGray.png")));
        L6.setIcon(new ImageIcon(getClass().getResource("Pictures/L6-lightGray.png")));
        R1.setIcon(new ImageIcon(getClass().getResource("Pictures/R1-lightGray.png")));
        R2.setIcon(new ImageIcon(getClass().getResource("Pictures/R2-lightGray.png")));
        R3.setIcon(new ImageIcon(getClass().getResource("Pictures/R3-lightGray.png")));
        R4.setIcon(new ImageIcon(getClass().getResource("Pictures/R4-lightGray.png")));
        R5.setIcon(new ImageIcon(getClass().getResource("Pictures/R5-lightGray.png")));
        R6.setIcon(new ImageIcon(getClass().getResource("Pictures/R6-lightGray.png")));

        clearSelectedZones();

        BarChart emptyChart = new BarChart("Measurements");
        ChartPanel.removeAll();
        ChartPanel.add(emptyChart.getChartPAnel(), BorderLayout.CENTER);
        ChartPanel.validate();

        zonesToSelect = false;
        createReportButton.setVisible(false);
        nocisenseLogoLabel.setVisible(true);
    }
    private static void clearWindowOfPatient() {
        PatientsChooser.setSelectedIndex(0);
        
        checkMarkFileLoaded.setVisible(false);
        
        measurementsInfoLabel.setText("");
        displayCprBottom.setText("");
        displayHealthConditionBottom.setText("");
    }
}
