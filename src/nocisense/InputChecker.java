package nocisense;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.swing.JOptionPane;

public class InputChecker {
    public static boolean integerChecker(String integer) {
        Pattern pattern = Pattern.compile("[\\d]{2,10}");
        Matcher mat = pattern.matcher(integer);
        return mat.matches();
    }
    
    public static boolean nameChecker(String name) {
        
            Pattern pattern = Pattern.compile("[A-Za-zÆæØøÅå\\s-]{1,30}");
            Matcher mat = pattern.matcher(name);
            return mat.matches();
       
        //ÆæØøÅå
       
    }
    public static boolean heightChecker(String height) {
        if (!height.equals("")) {
            Pattern pattern = Pattern.compile("[.\\d]{2,5}");
            Matcher mat = pattern.matcher(height);
            if (!mat.matches() || (Double.parseDouble(height) < 30.0) || 
                    (Double.parseDouble(height) > 250.0)) {
                JOptionPane.showMessageDialog(null, "Height must be in range:\n "
                        + "30.0cm > Height < 250.0cm","Invalid Height", JOptionPane.WARNING_MESSAGE);
            }  
            return mat.matches();
        }
        return true;
    }
    
    public static boolean weightChecker(String weight) {
        if (!weight.equals("")) {
            Pattern pattern = Pattern.compile("[.\\d]{1,5}");
            Matcher mat = pattern.matcher(weight);
            if (!mat.matches() || (Double.parseDouble(weight) < 7.0) || 
                    (Double.parseDouble(weight) > 700.0)) {
                JOptionPane.showMessageDialog(null, "Weight must be in range:\n "
                        + "7.0kg > Weight < 700.0kg","Invalid Weight", JOptionPane.WARNING_MESSAGE);
            }  
            return mat.matches();
        }
        return true;
    }
    
    public static boolean emailChecker(String email) {
        if (!email.equals("")) {
            Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
            Matcher mat = pattern.matcher(email);
            if (!mat.matches()) {
                JOptionPane.showMessageDialog(null, "E-mail must contain: @", "Invalid E-mail", 
                        JOptionPane.WARNING_MESSAGE);
            }
            return mat.matches();
        }
        return true;
    }
    public static boolean cprChecker(String CPR) {
        Pattern pattern = Pattern.compile("[\\d]{10}");
        Matcher mat = pattern.matcher(CPR);
        if (!mat.matches()) {
            JOptionPane.showMessageDialog(null, "CPR must be\n "
                    + "10 numbers long","Invalid CPR", JOptionPane.WARNING_MESSAGE);
        } 
        return mat.matches();
    }
    public static boolean phoneNoChecker(String phone) {
        if (!phone.equals("")) {
            Pattern pattern = Pattern.compile("[0-9+ ]{5,20}");
            Matcher mat = pattern.matcher(phone);
            if (!mat.matches()) {
                JOptionPane.showMessageDialog(null, "Phone # must be a number between "
                        + "5 and 20 characters long", "Invalid Phone #", JOptionPane.WARNING_MESSAGE);
            }
            return mat.matches();
        }
        return true;
    }
}