package nocisense;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;


public class DBActions extends Thread{
    
    
   
    // Deletes a patient directly from the database
 public static void deletePatient(Patient p) {
        try {
            Connection conn = DBConnector.getConnection();
            PreparedStatement pstmt = conn.prepareStatement("DELETE FROM patient WHERE idNo = ?");
            pstmt.setInt(1, p.getIdNo());
            pstmt.execute();
        } catch (SQLException ex) {
            System.err.println("Error deleting patient from database");
        }
        
    }
    // Saves patient to database
    public static void savePatient(Patient p) {
        try {
            Connection conn = DBConnector.getConnection();
            String sql = "INSERT INTO patient (cpr, name, address, phoneNo, "
                    + "email, weight, height, shoeSize, treatmentFacility, clinician, "
                    + "healthCondition) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, p.getCpr());
            pstmt.setString(2, p.getName());
            pstmt.setString(3, p.getAddress());
            pstmt.setString(4, p.getPhoneNo());
            pstmt.setString(5, p.getEmail());
            pstmt.setString(6, p.getWeight());
            pstmt.setString(7, p.getHeight());
            pstmt.setString(8, p.getShoeSize());
            pstmt.setString(9, p.getTreatmentFacility());
            pstmt.setString(10, p.getClinician());
            pstmt.setString(11, p.getHealthCondition());
            pstmt.execute();
        } catch (SQLException ex) {
            System.out.println(p);
            System.err.println("Error saving patient to database");
        }
    }
    // Updates patient info directl to database
    public static void updatePatient(Patient p) {
        try {
            Connection conn = DBConnector.getConnection();
            String sql = "UPDATE patient SET cpr=?, name=?, address=?, phoneNo=?, "
                    + "email=?, weight=?, height=?, shoeSize=?, treatmentFacility=?, "
                    + "clinician=?, healthCondition=? WHERE idNo=?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, p.getCpr());
            pstmt.setString(2, p.getName());
            pstmt.setString(3, p.getAddress());
            pstmt.setString(4, p.getPhoneNo());
            pstmt.setString(5, p.getEmail());
            pstmt.setString(6, p.getWeight());
            pstmt.setString(7, p.getHeight());
            pstmt.setString(8, p.getShoeSize());
            pstmt.setString(9, p.getTreatmentFacility());
            pstmt.setString(10, p.getClinician());
            pstmt.setString(11, p.getHealthCondition());
            pstmt.setInt(12, p.getIdNo());
            pstmt.execute();
        } catch (SQLException ex) {
            System.err.println("Error updating patient info in database");
        }
    }
    // Loads idNo as keys and name as values of a patients into a TreeMap from database
    public static TreeMap<Integer, String> loadPatientNames() {
        TreeMap<Integer, String> patientMap = new TreeMap<>();
        try {
            Connection conn = DBConnector.getConnection();
            String sql = "SELECT * FROM patient";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.executeQuery();
            ResultSet rs = pstmt.getResultSet();
            while (rs.next()) {
                int idNo = rs.getInt("idNo");
                String name = rs.getString("name");
                patientMap.put(idNo, name);
            }
        } catch (SQLException ex) {
            System.err.println("Error loading patients names from database");
        }
        return patientMap;
    }
    // Loads one patient's info from database into a patient object
    public static Patient loadPatientInfo(String name) {
        Patient p = new Patient();
        try {
            Connection conn = DBConnector.getConnection();
            String sql = "SELECT * FROM patient WHERE name = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.execute();
            ResultSet rs = pstmt.getResultSet();
            while(rs.next()) {
                int idNo = rs.getInt("idNo");
                String cpr = rs.getString("cpr");
                String address = rs.getString("address");
                String phoneNo = rs.getString("phoneNo");
                String email = rs.getString("email");
                String weight = rs.getString("weight");
                String height = rs.getString("height");
                String shoeSize = rs.getString("shoeSize");
                String treatmentFacility = rs.getString("treatmentFacility");
                String clinician = rs.getString("clinician");
                String healthCondition = rs.getString("healthCondition");
                p = new Patient(idNo, cpr, name, address, phoneNo, email, weight, 
                        height, shoeSize, treatmentFacility, clinician, healthCondition);
            }
            conn.close();
            pstmt.close();
            rs.close();
        } catch (SQLException ex) {
            System.err.println("Error loading patient info from database");
        }
        return p;
    }
    
    public static void saveTextFileIntoDatabase(String fileName,String patientName,String timeStamp) throws FileNotFoundException {
        try {
            Connection conn = DBConnector.getConnection();  // get connection to Database        
            conn.setAutoCommit(false); 
            File file = new File(fileName); // file to save into database
            FileInputStream fis = new FileInputStream(file);
            // insert int DB: measurements, patiend id(reference to patient) and timestamp
            PreparedStatement pstmt = conn.prepareStatement("insert into measurements"
            + "(patient_id, measurements,date) " 
            + "values ((SELECT idNo FROM patient WHERE name = ?), ?, ?)");
            pstmt.setString(1, patientName);
            pstmt.setAsciiStream(2, fis, (int) file.length());
            pstmt.setString(3, timeStamp);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            System.err.println("Problem with saving file into database");
        }
    }
     
    public static String getCLOB(String name, String timeStamp) throws Exception {
        String query = "SELECT measurements FROM measurements WHERE patient_id = (SELECT idNo FROM patient WHERE name = ?) AND date = ?";
        String wholeClob = "";
        try {
            Connection conn = DBConnector.getConnection();
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, name);
            pstmt.setString(2, timeStamp);
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            Clob clob = rs.getClob(1);
            wholeClob = clob.getSubString(1, (int) clob.length());//convert Clob into String
            rs.close();
            pstmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex.getErrorCode());
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
        }   
        return wholeClob;
    }

    public static int loadLastPatientID() {
        int lastID = 0;
        try { 
            Connection conn = DBConnector.getConnection();
            Statement myStmt = conn.createStatement(); 
            String query = "SELECT MAX(idNo) FROM patient";
            ResultSet rs = myStmt.executeQuery(query);        
            while(rs.next()){
               lastID = rs.getInt("MAX(idNo)");
            }  
            conn.close();
            myStmt.close();
            rs.close();
            return lastID;
             
        } catch (SQLException ex) {
            System.out.println(ex.getErrorCode());
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
        }
       return lastID;
    }
    
    public static ArrayList<String>getPatientNames() {
        ArrayList<String>getPatientNames = new ArrayList();
        
        try {
            Connection conn = DBConnector.getConnection();
            Statement myStmt = conn.createStatement(); 
            String query = "SELECT name FROM patient";
            ResultSet rs = myStmt.executeQuery(query);
            getPatientNames.add("");
            while(rs.next()){
                String name  = rs.getString("name");
                getPatientNames.add(name);
            }
            conn.close();
            myStmt.close();
            rs.close();   
        } catch (SQLException ex) {
            System.out.println(ex.getErrorCode());
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
        }
        return getPatientNames;
    }
    
    public static HashMap<String,ArrayList<String>> getPatientsAndMeasurements()
    {
        HashMap<String,ArrayList<String>>patientsAndMeasurements = new HashMap();
        
        ArrayList<String> PatientNames = getPatientNames();
        
        for(String patient: PatientNames )
        {
           patientsAndMeasurements.put(patient,getMeasurementsHistory(patient));
        }
        
        return patientsAndMeasurements;
    }
    
    public static ArrayList<String>getMeasurementsHistory(String name) { 
        ArrayList<String>getMeasurementsHistory = new ArrayList();
        try {
            Connection conn = DBConnector.getConnection();
            String query = "SELECT date FROM measurements WHERE patient_id =(SELECT idNo FROM patient WHERE name = ?)";
            PreparedStatement pstmt = conn.prepareStatement(query); 
            pstmt.setString(1, name);
            ResultSet rs = pstmt.executeQuery();

            getMeasurementsHistory.add("");
            while(rs.next()){
                String timeStamp  = rs.getString("date");
                getMeasurementsHistory.add(timeStamp);
            }
            conn.close();
            pstmt.close();
            rs.close();       
        } catch (SQLException ex) {
            System.out.println(ex.getErrorCode());
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
        }
        return getMeasurementsHistory;  
    } 

 
}