package nocisense;
import java.util.Objects;
/**
 *
 * @author Christina
 */
public class Patient {
    private int    idNo;
    private String cpr;
    private String name;
    private String address;
    private String phoneNo;
    private String email;
    private String weight;
    private String height;
    private String shoeSize;
    private String treatmentFacility;
    private String clinician;
    private String healthCondition;
    
    public Patient() {
        
    }
    
    public Patient(int idNo, String cpr, String name, String address, String phoneNo, 
            String email, String weight, String height, String shoeSize, 
            String treatmentFacility, String clinician, String healthCondition) {
        this.idNo = idNo;
        this.cpr = cpr;
        this.name = name;
        this.address = address;
        this.phoneNo = phoneNo;
        this.email = email;
        this.weight = weight;
        this.height = height;
        this.shoeSize = shoeSize;
        this.treatmentFacility = treatmentFacility;
        this.clinician = clinician;
        this.healthCondition = healthCondition;
    }
    
    @Override
    public String toString() {
        return "idNo: " + getIdNo() + ", cpr: " + getCpr() + ", name: " + getName() + ", address: " 
                + getAddress() + ", phoneNo: " + getPhoneNo() + ", email: " + getEmail() + ", weight: " 
                + getWeight() + ", height: " + getHeight() + ", shoeSize: " + getShoeSize() 
                + ", treatmentFacility: " + getTreatmentFacility() +  ", clinician: " 
                + getClinician() + " & healthCondition: " + getHealthCondition();
    }
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Patient) {
            Patient other = (Patient) obj;
            return idNo == other.idNo;
        } else {
            return false;
        }
    }
    public int getIdNo() {
        return idNo;
    }
    public void setIdNo(int idNo) {
        this.idNo = idNo;
    }
    public String getCpr() {
        return cpr;
    }
    public void setCpr(String cpr) {
        this.cpr = cpr;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getPhoneNo() {
        return phoneNo;
    }
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getWeight() {
        return weight;
    }
    public void setWeight(String weight) {
        this.weight = weight;
    }
    public String getHeight() {
        return height;
    }
    public void setHeight(String height) {
        this.height = height;
    }
    public String getShoeSize() {
        return shoeSize;
    }
    public void setShoeSize(String shoeSize) {
        this.shoeSize = shoeSize;
    }
    public String getTreatmentFacility() {
        return treatmentFacility;
    }
    public void setTreatmentFacility(String treatmentFacility) {
        this.treatmentFacility = treatmentFacility;
    }
    public String getClinician() {
        return clinician;
    }
    public void setClinician(String clinician) {
        this.clinician = clinician;
    }
    public String getHealthCondition() {
        return healthCondition;
    }
    public void setHealthCondition(String healthCondition) {
        this.healthCondition = healthCondition;
    }
}