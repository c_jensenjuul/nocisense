/*
In this class analysis of the data is done.
*/
package nocisense;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Christina
 */
public class Analyzer {
    // Creates a new HashMap with sensors as keys, of the amount of values that are above a given high
    // threshold and the amount of values lover than the high threshold, but higher than the low threshold
    public static HashMap<String, ArrayList<Integer>> getCounts(HashMap<String, 
            ArrayList<Double>> dataMap, int lowThreshold, int highThreshold) {  
        HashMap<String, ArrayList<Integer>> mapCounts = new HashMap<>(); 
        for(String sensor : dataMap.keySet()) {//for each sensor: L1,L2,L3......R6
            ArrayList<Double> valueList = dataMap.get(sensor); // get values for current sensor
            ArrayList<Integer> counts = new ArrayList<>();
            int lowCount = 0;
            int highCount = 0;
            for(Double value : valueList) {  
                if(value > highThreshold) {//if value is larger than High Threshold
                    highCount++; // add 1 to High counter
                } else if (value > lowThreshold) {//if value is larger than Low Threshold
                    lowCount++; // add 1 to Low counter
                } 
            }
            counts.add(highCount);
            counts.add(lowCount);
            mapCounts.put(sensor, counts);
        }
        return mapCounts;
    }   
}